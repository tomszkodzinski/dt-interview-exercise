# Sabre UI Developer Interview Exercise

## Online preview

http://tgfg.pl/works/sabre/

## Application info & Setting up

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Bundle size: 194 KB (gzipped: 61 KB).

To run the App locally do:

> `yarn install`  
> `yarn start`

or

> `npm install`  
> `npm start`

and visit http://localhost:3000/

## Additional libraries

Except for the use of `react-jss` to describe styles with JavaScript and the `Classnames` for conditionally joining css classes, no additional libraries were installed.

## Styling

All styles were custom written. CSS-in-JS (JSS) was used, mainly because of its dynamic nature and as preparation for possible addition of theming, dark theme etc.

## State management

As I did't come across prop drilling problem - there is no React useContext / useReducer or Redux solutions involved, just simple passing props from parent to child.

## Deficiencies in the solution / doubts

- There is no animation applied to the 'top sales' icon. I planned to use [GSAP](https://greensock.com/gsap/) as I have some experience with animation using this library. Problems with GSAP Typescript integration and lack of time led to unfinishing this task.
- The "Top Performers" section is filter sensitive, but I'm not sure if the task creators meant it. This is useful for the "company name" filter, but doesn't make much sense for the "minimum sales" filter.

## Further development ideas

- ARIA attributes and accessibility practices should be implemented.
- Keyboard support should be improved (e.g. submit on Enter key).
- Integration and unit tests should be added.
- Addition of some advanced state management tool would be good basis for further development (I'd probably try Recoil over Redux).
- Custom hooks were put in `shared` folder, but there should be more work involved to make them 100% reusable, as for the moment their argument types are associated with sales data.
- I used native `<input type="range">` range slider. But it has some styling issues (cross browsers hacks, lack of styles on mobile) and for production it should be replaced with custom written component or well tested component like [Slider](https://material-ui.com/components/slider/) from Material-UI.
