import { FC } from 'react';
import { Paper, Table } from '../../../../shared/ui';
import { SalesDataItem } from '../../../../shared/hooks/useFetchSalesData';

interface Props {
  data?: SalesDataItem[];
}

const TopPerformers: FC<Props> = ({ data }) => {
  const salesDataEmpty = !data?.length;
  const topPerformers = data?.filter((item) => item.sales >= 800);
  const topPerformanceCount = topPerformers?.length;
  const topPerformanceCountPercentage =
    topPerformanceCount && data?.length ? Math.floor((topPerformanceCount / data?.length) * 100) : null;
  const totalSales = topPerformers?.reduce((acc, current) => acc + current.sales, 0) ?? 0;
  const avarageSales = topPerformanceCount ? Math.round(totalSales / topPerformanceCount) : 0;

  return (
    <>
      <h2>Top Performers ($800+ / month)</h2>
      <Paper>
        <Table dataEmpty={salesDataEmpty}>
          <tbody>
            <tr>
              <td>Number of Clients</td>
              <td>
                {topPerformanceCount}{' '}
                <small>{topPerformanceCountPercentage ? `(${topPerformanceCountPercentage}%)` : null}</small>
              </td>
            </tr>
            <tr>
              <td>Average Monthly Sales</td>
              <td>${avarageSales}</td>
            </tr>
          </tbody>
        </Table>
      </Paper>
    </>
  );
};

export default TopPerformers;
