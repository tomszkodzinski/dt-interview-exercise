import { FC } from 'react';
import { SalesDataItem } from '../../../../shared/hooks/useFetchSalesData';
import { Button, Paper, Table, Pagination } from '../../../../shared/ui';
import { useStyles } from './styles';
import { formatNumber } from '../../../../shared/utils/utils';
import usePagination from '../../../../shared/hooks/usePagination';
import { TrophieIcon } from '../../../../shared/icons';

interface Props {
  data: SalesDataItem[];
  topSales?: number[];
  refetchData: () => void;
}

const POSTS_PER_PAGE = 10;

const SalesDataTable: FC<Props> = ({ data, topSales, refetchData }) => {
  const classes = useStyles();
  const { currentData, goToPage, currentPage } = usePagination(data, POSTS_PER_PAGE);

  const pagesCount = Math.ceil(data.length / POSTS_PER_PAGE);
  const totalSales = Math.round(data?.reduce((acc, current) => acc + current.sales, 0) ?? 0);
  const pageSalesSubtotal = Math.round(currentData()?.reduce((acc, current) => acc + current.sales, 0) ?? 0);
  const salesDataEmpty = !data?.length;

  return (
    <div className="sales-data">
      <div className={classes.toolbar}>
        <h2>Sales Data</h2>
        <Button onClick={refetchData}>Refresh data</Button>
      </div>
      <Paper>
        <Table dataEmpty={salesDataEmpty}>
          <thead>
            <tr>
              <th>Name</th>
              <th>Company</th>
              <th>Monthly sales</th>
            </tr>
          </thead>
          <tbody>
            {currentData().map((item) => (
              <tr key={item.id}>
                <td>
                  {topSales?.includes(item.id) ? (
                    <div className={classes.trophieIconContainer}>
                      <TrophieIcon />
                    </div>
                  ) : null}
                  {item.name}
                </td>
                <td>{item.company}</td>
                <td>{item.sales}</td>
              </tr>
            ))}
          </tbody>
          <tfoot>
            <tr>
              <td colSpan={2}>Page Sales Subtotal</td>
              <td>{formatNumber(pageSalesSubtotal)}</td>
            </tr>
            <tr>
              <td colSpan={2}>Total Sales</td>
              <td>{formatNumber(totalSales)}</td>
            </tr>
          </tfoot>
        </Table>
      </Paper>
      {!salesDataEmpty ? (
        <Pagination count={pagesCount} currentPage={currentPage} handlePageChange={(page) => goToPage(page)} />
      ) : null}
    </div>
  );
};

export default SalesDataTable;
