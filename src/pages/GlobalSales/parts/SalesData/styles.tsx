import { createUseStyles } from 'react-jss';

export const useStyles = createUseStyles({
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  trophieIconContainer: {
    position: 'absolute',
    width: 24,
    height: 24,
    background: '#fff',
    left: -12,
    top: 0,
    bottom: 0,
    margin: 'auto 0',
    padding: 1,
    boxShadow: '0 2px 0 0 #e8e8e8',
    borderRadius: '50%',

    '& svg': {
      fill: '#e0a30a',
    },
  },
});
