import { FC, useState } from 'react';
import { Button, Input, Paper, RangeSlider } from '../../../../shared/ui';

export type FiltersDto = {
  companyNameFilter?: string;
  minSalesFilter?: number;
};

interface Props {
  handleChangeFilters: ({ companyNameFilter, minSalesFilter }: FiltersDto) => void;
}

const Filters: FC<Props> = ({ handleChangeFilters }) => {
  const [companyNameFilter, setCompanyNameFilter] = useState('');
  const [minSalesFilter, setMinSalesFilter] = useState<number>();

  const handleCompanyFilterChange = (event: React.FormEvent<HTMLInputElement>) => {
    setCompanyNameFilter(event.currentTarget.value);
  };

  const handleMinSalesFilterChange = (minSales: number) => {
    setMinSalesFilter(minSales);
  };

  return (
    <Paper hasPadding>
      <Input onChange={handleCompanyFilterChange} fullWidth placeholder="Company" />
      <RangeSlider
        label="Minimum Sales ($)"
        min={0}
        max={1700}
        initialValue={250}
        value={minSalesFilter}
        onChange={handleMinSalesFilterChange}
      />
      <Button primary fullWidth onClick={() => handleChangeFilters({ companyNameFilter, minSalesFilter })}>
        Filter results
      </Button>
    </Paper>
  );
};

export default Filters;
