import { FC, useEffect, useMemo, useState } from 'react';
import SalesData from './parts/SalesData';
import TopPerformers from './parts/TopPerformers';
import Filters from './parts/Filters';
import { Spinner } from '../../shared/ui';
import { FiltersDto } from './parts/Filters/Filters';
import useFetchSalesData, { SalesDataItem } from '../../shared/hooks/useFetchSalesData';

const GlobalSales: FC = () => {
  const { salesData, fetchSalesData, loading } = useFetchSalesData([]);
  const [filteredSalesData, setFilteredSalesData] = useState<SalesDataItem[]>([]);
  const [filters, setFilters] = useState<FiltersDto>({});

  useEffect(() => {
    const newFilteredSalesData = salesData
      .filter((item) => item.company.toLowerCase().indexOf(filters.companyNameFilter?.toLowerCase() ?? '') !== -1)
      .filter((item) => item.sales >= (filters.minSalesFilter ?? 250));

    setFilteredSalesData(newFilteredSalesData);
  }, [salesData, filters]);

  const handleChangeFilters = (filters: FiltersDto) => {
    setFilters(filters);
  };

  const topSales = useMemo(() => {
    const topSalesSorted = [...salesData].sort((a, b) => b.sales - a.sales);
    const topSalesIds = topSalesSorted.slice(0, 3).map((topSale) => topSale.id);

    return topSalesIds;
  }, [salesData]);

  return (
    <>
      <h1>Global Sales</h1>
      <Filters handleChangeFilters={handleChangeFilters} />
      <SalesData data={filteredSalesData} refetchData={fetchSalesData} topSales={topSales} />
      <TopPerformers data={filteredSalesData} />
      {loading && <Spinner />}
    </>
  );
};

export default GlobalSales;
