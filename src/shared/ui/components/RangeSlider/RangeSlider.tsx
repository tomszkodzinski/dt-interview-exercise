import { FC } from 'react';
import Input from '../Input';
import { useStyles } from './styles';

interface Props {
  label?: string;
  initialValue?: number;
  min?: number;
  max?: number;
  value?: number;
  onChange: (value: number) => void;
}

const RangeSlider: FC<Props> = ({ label, initialValue = 250, min = 0, max = 1500, value, onChange }) => {
  const classes = useStyles();

  const handleValueChange = (event: React.FormEvent<HTMLInputElement>, limit = false) => {
    let value = parseInt(event.currentTarget.value, 10);

    if (isNaN(value)) {
      value = 0;
    }

    if (limit) {
      if (value < min) {
        value = min;
      } else if (value > max) {
        value = max;
      }
    }

    onChange(value);
  };

  return (
    <div className={classes.rangeSliderContainer}>
      <div>
        <label>{label}</label>
        <input
          className={classes.rangeSlider}
          type="range"
          value={value !== undefined ? value : initialValue}
          min={min}
          max={max}
          onChange={(event) => handleValueChange(event)}
        />
      </div>
      <Input
        dense
        value={value !== undefined ? value : initialValue}
        className={classes.rangeSliderValue}
        onChange={(event) => handleValueChange(event, true)}
      />
    </div>
  );
};

export default RangeSlider;
