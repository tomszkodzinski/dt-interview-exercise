import { createUseStyles } from 'react-jss';
import { themeConsts } from '../../../styles/baselineStyles';

export const useStyles = createUseStyles({
  rangeSliderValue: {
    fontWeight: '700',
    textAlign: 'center',
    width: 100,
    marginLeft: 16,
    marginBottom: 4,
  },
  rangeSliderContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    margin: '18px 0px',

    '& > div': {
      flex: 1,
    },

    '& label': {
      marginBottom: 4,
      display: 'block',
    },
  },

  // Firefox
  rangeSlider: {
    margin: '18px 0',
    height: 8,

    /* fix for FF unable to apply focus style bug  */
    border: '1px solid white',

    /*required for proper track sizing in FF*/
    width: '100%',

    '&::-moz-range-track': {
      width: '100%',
      height: 6,
      background: 'rgba(0,0,0,0.2)',
      border: 'none',
      borderRadius: 4,
    },

    '&::-moz-range-thumb': {
      border: 'none',
      height: 24,
      width: 24,
      borderRadius: '50%',
      background: themeConsts.primaryLightColor,
      cursor: 'pointer',
    },

    /*hide the outline behind the border*/
    '&:-moz-focusring': {
      outline: '1px solid white',
      outlineOffset: -1,
    },

    '&:focus::-moz-range-track': {
      background: 'rgba(0,0,0,0.2)',
    },

    '&::-moz-range-progress': {
      height: 6,
      backgroundColor: themeConsts.primaryLightColor,
      border: 'none',
      borderRadius: 4,
    },
  },

  // Chrome
  //https://stackoverflow.com/questions/18389224/how-to-style-html5-range-input-to-have-different-color-before-and-after-slider
  '@media screen and (-webkit-min-device-pixel-ratio:0) and (min-resolution:.001dpcm)': {
    rangeSlider: {
      WebkitAppearance: 'none',
      background: themeConsts.primaryLightColor,
      height: 8,
      borderRadius: 4,

      '&::-webkit-slider-runnable-track': {
        WebkitAppearance: 'none',
        color: 'red',
        width: '100%',
        height: 6,
        background: 'themeConsts.primaryLightColor',
        border: 'none',
        borderRadius: 4,
      },

      '&::-webkit-slider-thumb': {
        WebkitAppearance: 'none',
        border: 'none',
        cursor: 'pointer',
        height: 24,
        width: 24,
        borderRadius: '50%',
        background: themeConsts.primaryLightColor,
        marginTop: -10,
      },

      '&:focus': {
        outline: 'none',
      },

      '&:focus::-webkit-slider-runnable-track': {
        background: themeConsts.primaryLightColor,
      },
    },
  },
});
