import { FC, ReactNode } from 'react';
import { useStyles } from './styles';

interface Props {
  children?: ReactNode;
}

const Container: FC<Props> = ({ children }) => {
  const classes = useStyles();

  return <div className={classes.container}>{children}</div>;
};

export default Container;
