import { createUseStyles } from 'react-jss';

export const useStyles = createUseStyles({
  container: {
    width: '100%',
    maxWidth: 640,
    margin: '0 auto',
  },
});
