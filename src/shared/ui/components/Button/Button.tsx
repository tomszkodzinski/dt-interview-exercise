import { FC, ReactNode } from 'react';
import { useStyles } from './styles';
import classNames from 'classnames';

interface Props {
  children?: ReactNode;
  primary?: boolean;
  fullWidth?: boolean;
  onClick?: () => void;
}

const Button: FC<Props> = ({ children, primary, fullWidth, onClick }) => {
  const classes = useStyles();

  return (
    <button
      onClick={onClick}
      className={classNames(classes.button, {
        [classes.primaryButton]: primary,
        [classes.fullWidth]: fullWidth,
      })}
    >
      {children}
    </button>
  );
};

export default Button;
