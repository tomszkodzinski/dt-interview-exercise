import { createUseStyles } from 'react-jss';
import { themeConsts } from '../../../styles/baselineStyles';

export const useStyles = createUseStyles({
  button: {
    backgroundColor: 'transparent',
    transition: 'background-color 0.4s ease',
    fontFamily: themeConsts.headlineFontFamily,
    textTransform: 'uppercase',
    fontSize: 16,
    fontWeight: 700,
    borderRadius: themeConsts.borderRadius,
    padding: '9px 14px',
    border: 0,
    color: themeConsts.textColor,
    boxShadow: 'none',
    cursor: 'pointer',

    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.07)',
    },
  },
  primaryButton: {
    backgroundColor: '#3d3f44',
    color: '#fff',
    boxShadow: 'inset 0 0 0 3px #121315',

    '&:hover': {
      backgroundColor: '#121315',
    },
  },
  fullWidth: {
    width: '100%',
  },
});
