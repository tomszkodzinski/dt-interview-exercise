import { createUseStyles } from 'react-jss';
import { themeConsts } from '../../../styles/baselineStyles';

export const useStyles = createUseStyles({
  noData: {
    padding: 40,
    textAlign: 'center',
    color: 'rgba(0, 0, 0, 0.5)',
  },
  table: {
    borderCollapse: 'collapse',
    borderSpacing: 0,
    margin: 0,
    padding: 0,
    width: '100%',

    '& tbody, & tfoot, & thead, & tr, & th, & td': {
      margin: 0,
      padding: 0,
      border: 0,
    },

    '& thead': {
      borderBottom: `2px solid ${themeConsts.dividerColor}`,

      '& th': {
        color: '#6d7178',
        fontSize: 16,
        fontWeight: 700,
        textTransform: 'uppercase',
      },
    },

    '& td, & th': {
      textAlign: 'left',
      padding: 14,
      position: 'relative',

      '&:first-child': {
        paddingLeft: 18,
      },

      '&:last-child': {
        textAlign: 'right',
        paddingRight: 18,
      },
    },

    '& tfoot': {
      borderTop: `1px solid ${themeConsts.dividerColor}`,
      color: '#404248',
      fontWeight: 700,

      '& td': {
        paddingTop: 2,
        paddingBottom: 2,
        backgroundColor: themeConsts.backgroundColor,
      },

      '& tr:first-child td': {
        paddingTop: 14,
      },

      '& tr:last-child td': {
        paddingBottom: 14,

        '&:first-child': {
          borderBottomLeftRadius: 12,
        },

        '&:last-child': {
          borderBottomRightRadius: 12,
        },
      },

      '&:last-child': {
        textAlign: 'right',
      },
    },

    '& tbody tr': {
      borderBottom: `1px solid ${themeConsts.dividerColor}`,

      '&:last-child': {
        borderBottom: 0,
      },
    },

    '& tbody tr td:last-child, & tfoot tr td:last-child': {
      fontFamily: themeConsts.numbersFontFamily,
    },
  },
});
