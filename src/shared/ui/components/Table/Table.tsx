import { FC, ReactNode } from 'react';
import { useStyles } from './styles';

interface Props {
  children?: ReactNode;
  dataEmpty?: boolean;
}

const Table: FC<Props> = ({ children, dataEmpty }) => {
  const classes = useStyles();

  if (dataEmpty) {
    return <div className={classes.noData}>No results</div>;
  }

  return <table className={classes.table}>{children}</table>;
};

export default Table;
