import { createUseStyles } from 'react-jss';
import { themeConsts } from '../../../styles/baselineStyles';

export const useStyles = createUseStyles({
  '@keyframes spin': {
    from: { transform: 'rotate(0deg)' },
    to: { transform: 'rotate(360deg)' },
  },
  '@keyframes fadeIn': {
    from: { opacity: 0 },
    to: { topacity: 1 },
  },
  spinner: {
    border: '6px solid #dcdede',
    borderRadius: '50%',
    borderTop: `6px solid ${themeConsts.primaryColor}`,
    width: 50,
    height: 50,
    animation: '$spin 2s linear infinite',
  },
  spinnerContainer: {
    position: 'fixed',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(246,246,247,0.8)',
    animation: '$fadeIn 0.2s linear',
  },
});
