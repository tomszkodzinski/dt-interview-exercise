import { FC } from 'react';
import { useStyles } from './styles';

const Spinner: FC = () => {
  const classes = useStyles();

  return (
    <div className={classes.spinnerContainer}>
      <div className={classes.spinner} />
    </div>
  );
};

export default Spinner;
