import { FC } from 'react';
import { useStyles } from './styles';
import { PaginationForwardIcon, PaginationBackwardIcon } from '../../../icons';
import classNames from 'classnames';

interface Props {
  count: number;
  currentPage: number;
  handlePageChange: (pageNumber: number) => void;
}

const Pagination: FC<Props> = ({ count, currentPage, handlePageChange }) => {
  const classes = useStyles();

  return (
    <div className={classes.paginationContainer}>
      <div>
        <div
          onClick={() => handlePageChange(currentPage - 1)}
          className={classNames(classes.paginationButton, { [classes.disabled]: currentPage === 1 })}
        >
          <PaginationBackwardIcon />
        </div>
        <div className={classes.pageNumbersContainer}>
          {Array.from({ length: count }).map((_, index) => (
            <div
              onClick={() => handlePageChange(index + 1)}
              key={index}
              className={classNames(classes.paginationButton, {
                [classes.activePageNumber]: currentPage === index + 1,
              })}
            >
              {index + 1}
            </div>
          ))}
        </div>
        <div
          onClick={() => handlePageChange(currentPage + 1)}
          className={classNames(classes.paginationButton, { [classes.disabled]: currentPage === count })}
        >
          <PaginationForwardIcon />
        </div>
      </div>
    </div>
  );
};

export default Pagination;
