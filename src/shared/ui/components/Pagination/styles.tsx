import { createUseStyles } from 'react-jss';
import { themeConsts } from '../../../styles/baselineStyles';

export const useStyles = createUseStyles({
  paginationContainer: {
    textAlign: 'center',

    '& > div': {
      maxWidth: '80%',
      margin: '12px 0',
      display: 'inline-flex',
      alignItems: 'center',
    },
  },
  pageNumbersContainer: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  paginationButton: {
    borderRadius: '50%',
    width: 36,
    height: 36,
    display: 'flex',
    color: themeConsts.primaryColor,
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 700,
    margin: '0 2px',
    cursor: 'pointer',
    transition: 'background-color 0.4s ease',
    flexShrink: 0,

    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0.07)',
    },

    '& > svg path': {
      fill: themeConsts.primaryColor,
    },
  },
  activePageNumber: {
    backgroundColor: themeConsts.primaryColor,
    color: '#fff',
    cursor: 'default',
    pointerEvent: 'none',

    '&:hover': {
      backgroundColor: themeConsts.primaryColor,
    },
  },
  disabled: {
    cursor: 'default',
    pointerEvents: 'none',

    '& > svg path': {
      fill: 'rgba(0, 0, 0, 0.3)',
    },

    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
});
