import { createUseStyles } from 'react-jss';
import { themeConsts } from '../../../styles/baselineStyles';

export const useStyles = createUseStyles({
  paper: {
    backgroundColor: '#fff',
    borderRadius: themeConsts.borderRadius * 2,
    boxShadow: '0 0 12px 0 rgba(0,0,0,0.07)',
    marginBottom: 8,
  },
  paperPadding: {
    padding: 24,
  },
});
