import { FC, ReactNode } from 'react';
import { useStyles } from './styles';
import classNames from 'classnames';

interface Props {
  children?: ReactNode;
  hasPadding?: boolean;
}

const Paper: FC<Props> = ({ children, hasPadding }) => {
  const classes = useStyles();

  return (
    <div
      className={classNames(classes.paper, {
        [classes.paperPadding]: hasPadding,
      })}
    >
      {children}
    </div>
  );
};

export default Paper;
