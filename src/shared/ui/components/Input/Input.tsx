import { FC } from 'react';
import { useStyles } from './styles';
import classNames from 'classnames';

interface Props {
  value?: string | number;
  onChange?: (event: React.FormEvent<HTMLInputElement>) => void;
  dense?: boolean;
  placeholder?: string;
  fullWidth?: boolean;
  className?: string;
}

const Input: FC<Props> = ({ value, onChange, dense, placeholder, fullWidth, className }) => {
  const classes = useStyles();

  return (
    <input
      value={value}
      type="text"
      onChange={onChange}
      placeholder={placeholder}
      className={classNames(className, classes.input, {
        [classes.dense]: dense,
        [classes.fullWidth]: fullWidth,
      })}
    />
  );
};

export default Input;
