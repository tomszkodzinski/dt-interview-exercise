import { createUseStyles } from 'react-jss';
import { themeConsts } from '../../../styles/baselineStyles';

export const useStyles = createUseStyles({
  input: {
    borderRadius: themeConsts.borderRadius * 2,
    border: '1px solid #6d7178',
    padding: '20px 12px',
    fontSize: themeConsts.fontSize,
    fontFamily: themeConsts.fontFamily,
    backgroundColor: themeConsts.backgroundColor,
    transition: 'all 0.3s ease',
    WebkitAppearance: 'none',

    '&:focus-visible': {
      outline: 0,
    },

    '&:focus, &:active': {
      border: `1px solid ${themeConsts.primaryColor}`,
      boxShadow: `inset 0 0 0 1px ${themeConsts.primaryColor}`,
      outline: 0,
    },
  },
  dense: {
    borderRadius: themeConsts.borderRadius,
    padding: '8px 6px',
  },
  fullWidth: {
    width: '100%',
  },
});
