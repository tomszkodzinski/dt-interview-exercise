import { useCallback, useEffect, useState } from 'react';
import { SalesDataItem } from './useFetchSalesData';

function usePagination(data: SalesDataItem[], itemsPerPage: number) {
  const [currentPage, setCurrentPage] = useState(1);

  const maxPage = Math.ceil(data.length / itemsPerPage);

  const currentData = () => {
    const begin = (currentPage - 1) * itemsPerPage;
    const end = begin + itemsPerPage;

    return data.slice(begin, end);
  };

  const goToPage = useCallback(
    (page: number) => {
      const pageNumber = Math.max(1, page);
      setCurrentPage(Math.min(pageNumber, maxPage));
    },
    [maxPage],
  );

  useEffect(() => {
    if (maxPage && currentPage > maxPage) {
      setCurrentPage(maxPage);
      goToPage(maxPage);
    }
  }, [currentPage, goToPage, maxPage]);

  return { goToPage, currentData, currentPage };
}

export default usePagination;
