import { useEffect, useState } from 'react';

export interface SalesDataItem {
  id: number;
  name: string;
  company: string;
  sales: number;
}

const responseTime = 1200;
async function simulateResponseTime(ms: number) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

export default function useFetchSalesData(initialValue: SalesDataItem[]): {
  salesData: SalesDataItem[];
  fetchSalesData: () => void;
  loading: boolean;
} {
  const [salesData, setSalesData] = useState(initialValue);
  const [loading, setLoading] = useState(false);

  const fetchSalesData = async () => {
    try {
      setLoading(true);

      const response = await fetch('./mock_data.json');
      const data = await response.json();
      await simulateResponseTime(responseTime);

      setLoading(false);
      setSalesData([...data]);
    } catch (err) {
      // server error handling
    }
  };

  useEffect(() => {
    fetchSalesData();
  }, []);

  return { salesData, fetchSalesData, loading };
}
