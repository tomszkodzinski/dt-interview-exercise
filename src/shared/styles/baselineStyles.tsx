import { createUseStyles } from 'react-jss';

export const themeConsts = {
  fontFamily: "'Roboto', Arial, 'sans-serif'",
  headlineFontFamily: "'Roboto Condensed', Arial, 'sans-serif'",
  numbersFontFamily: "'Roboto Mono', monospace",
  fontSize: 16,
  textColor: '#2e3034',
  borderRadius: 6,
  backgroundColor: '#f6f6f7',
  primaryColor: '#007b94',
  primaryLightColor: '#5498b2',
  dividerColor: '#e1e2e5',
};

export const useBaselineStyles = createUseStyles({
  '@global': {
    html: {
      WebkitFontSmoothing: 'antialiased',
      MozOsxFontSmoothing: 'grayscale',
      boxSizing: 'border-box',
      WebkitTextSizeAdjust: '100%',
      fontFamily: themeConsts.fontFamily,
    },
    '*, *::before, *::after': {
      boxSizing: 'inherit',
    },
    body: {
      backgroundColor: themeConsts.backgroundColor,
      fontSize: themeConsts.fontSize,
      margin: '0 24px 24px 24px',
      color: themeConsts.textColor,
    },
    h1: {
      fontFamily: themeConsts.headlineFontFamily,
      fontSize: 31,
      padding: '16px 0',
      margin: 0,
    },
    h2: {
      fontFamily: themeConsts.headlineFontFamily,
      fontSize: 25,
      padding: '18px 0',
      margin: 0,
    },
  },
});
