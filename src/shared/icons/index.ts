export { default as PaginationForwardIcon } from './PaginationForwardIcon';
export { default as PaginationBackwardIcon } from './PaginationBackwardIcon';
export { default as TrophieIcon } from './TrophieIcon';
