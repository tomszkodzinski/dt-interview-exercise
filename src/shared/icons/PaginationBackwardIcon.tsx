import { FC } from 'react';

const PaginationBackwardIcon: FC = () => {
  return (
    <svg width="16" height="16" viewBox="0 0 45 45">
      <path d="M11.5,23.3L29.2,5.6L31.6,8L16.3,23.4l15.3,15.3l-2.5,2.5L11.5,23.4V23.3L11.5,23.3z" />
    </svg>
  );
};

export default PaginationBackwardIcon;
