import { FC } from 'react';

const PaginationForwardIcon: FC = () => {
  return (
    <svg width="16" height="16" viewBox="0 0 45 45">
      <path d="M34.5,23.4L34.5,23.4L17,41.1l-2.5-2.5l15.3-15.3L14.5,8L17,5.6L34.5,23.4L34.5,23.4z" />
    </svg>
  );
};

export default PaginationForwardIcon;
