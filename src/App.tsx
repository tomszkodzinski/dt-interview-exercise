import { useBaselineStyles } from './shared/styles/baselineStyles';
import GlobalSales from './pages/GlobalSales';
import { Container } from './shared/ui';

function App() {
  useBaselineStyles();

  return (
    <Container>
      <GlobalSales />
    </Container>
  );
}

export default App;
